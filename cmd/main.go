package main

import (
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	http2 "julo/internal/adapter/http"
	"julo/internal/adapter/repository"
	"julo/internal/config"
	"julo/internal/service"
	"net/http"
)

func main() {
	confg := config.New()
	masterBD := config.SetupDB(confg)
	walletRepo := repository.NewWalletRepository(masterBD)
	depositRepo := repository.NewDepositRepository(masterBD)
	withdrawalRepo := repository.NewWithdrawalRepository(masterBD)
	walletSvc := service.NewWalletService(walletRepo)
	depositSvc := service.NewDepositService(walletRepo, depositRepo)
	withdrawalSvc := service.NewWithdrawalService(walletRepo, withdrawalRepo)
	hdlr := http2.NewHandler(walletSvc, depositSvc, withdrawalSvc)

	muxRouter := mux.NewRouter()
	muxRouter.Use(service.MiddlewareJWTAuthorization)
	http2.SetupRouter(muxRouter, hdlr)
	httpServer := http.Server{
		Addr:    fmt.Sprintf(":%d", confg.Port),
		Handler: muxRouter,
	}
	httpServer.ListenAndServe()

}
