# mini-wallet service

## Stack
- Golang 1.18
- PostgreSQL

### Current releases
| Version    | Link                                                  |
| ---------- |-------------------------------------------------------|
| Local      | http://localhost:8211/                                |


## Running this application
- Run following commands
    ```
    # Create db connection in PostgreSQL
    - open config.local.yaml
    - adjust host, port, dbName, user, password

    # Make sure Table already exist
    - execute query in package db/create.sql
  
    # Running the apps
    - running di application through run func main() under package cmd/main.go
    ```

