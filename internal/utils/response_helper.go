package utils

import (
	"encoding/json"
	"github.com/google/uuid"
	"julo/internal/model"
	"net/http"
)

func RespondError(w http.ResponseWriter, statusCode int, message string) {
	bt, _ := json.Marshal(model.Response{
		Status:  statusCode,
		Message: message,
		Data:    nil,
	})

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(bt)
	return
}

func RespondSuccess(w http.ResponseWriter, statusCode int, data interface{}) {
	bt, _ := json.Marshal(model.Response{
		Status:  200,
		Message: "Success",
		Data:    data,
	})

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(bt)
	return
}

func IsValidUUID(u string) bool {
	_, err := uuid.Parse(u)
	return err == nil
}
