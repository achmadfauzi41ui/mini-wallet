package repository

import (
	"context"
	"github.com/jmoiron/sqlx"
	"julo/internal/model/entity"
)

type withdrawalRepository struct {
	db *sqlx.DB
}

func NewWithdrawalRepository(db *sqlx.DB) WithdrawalRepository {
	return &withdrawalRepository{db: db}
}

type WithdrawalRepository interface {
	Create(ctx context.Context, withdrawal entity.Withdrawal) error
	CheckReferenceId(ctx context.Context, referenceId string) (count int, err error)
}

func (s *withdrawalRepository) Create(ctx context.Context, withdrawal entity.Withdrawal) error {
	sqlStr := `INSERT INTO withdrawal ("id","withdrawn_by","status","withdrawn_at", amount, reference_id) VALUES ($1, $2, $3, $4, $5, $6)`
	var args []interface{}
	args = append(args, withdrawal.Id, withdrawal.WithdrawnBy, withdrawal.Status, withdrawal.WithdrawnAt, withdrawal.Amount, withdrawal.ReferenceId)
	stmt, err := s.db.PrepareContext(ctx, sqlStr)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, args...)
	if err != nil {
		return err
	}
	return nil
}

func (s *withdrawalRepository) CheckReferenceId(ctx context.Context, referenceId string) (count int, err error) {
	queryStr := `
	Select 
	    count(wi.id)
	from withdrawal wi 
	where wi.reference_id = $1`

	stmt, err := s.db.PrepareContext(ctx, queryStr)
	if err != nil {
		return 0, err
	}
	err = stmt.QueryRowContext(ctx, referenceId).Scan(
		&count)
	if err != nil {
		return 0, err
	}
	return count, nil
}
