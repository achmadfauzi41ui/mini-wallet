package repository

import (
	"context"
	"github.com/jmoiron/sqlx"
	"julo/internal/model/entity"
)

type depositRepository struct {
	db *sqlx.DB
}

func NewDepositRepository(db *sqlx.DB) DepositRepository {
	return &depositRepository{db: db}
}

type DepositRepository interface {
	Create(ctx context.Context, deposit entity.Deposit) error
	CheckReferenceId(ctx context.Context, referenceId string) (int, error)
}

func (s *depositRepository) Create(ctx context.Context, deposit entity.Deposit) error {
	sqlStr := `INSERT INTO deposit ("id", "deposited_by", "status","deposited_at", amount, reference_id) VALUES ($1, $2, $3, $4, $5, $6)`
	var args []interface{}
	args = append(args, deposit.Id, deposit.DepositedBy, deposit.Status, deposit.DepositedAt, deposit.Amount, deposit.ReferenceId)
	stmt, err := s.db.PrepareContext(ctx, sqlStr)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, args...)
	if err != nil {
		return err
	}
	return nil
}

func (s *depositRepository) CheckReferenceId(ctx context.Context, referenceId string) (count int, err error) {
	queryStr := `
	Select 
	    count(de.id)
	from deposit de 
	where de.reference_id = $1`

	stmt, err := s.db.PrepareContext(ctx, queryStr)
	if err != nil {
		return 0, err
	}
	err = stmt.QueryRowContext(ctx, referenceId).Scan(
		&count)
	if err != nil {
		return 0, err
	}
	return count, nil
}
