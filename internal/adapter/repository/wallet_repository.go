package repository

import (
	"context"
	"github.com/jmoiron/sqlx"
	"julo/internal/model/entity"
	"time"
)

type walletRepository struct {
	db *sqlx.DB
}

type WalletRepository interface {
	Create(ctx context.Context, account entity.Wallet) error
	Retrieve(ctx context.Context, username string) (entity.Wallet, error)
	UpdateWallet(ctx context.Context, customerId string, time time.Time, status bool) error
	UpdateBalanceWallet(ctx context.Context, balance int32, customerId string) error
}

func NewWalletRepository(db *sqlx.DB) WalletRepository {
	return &walletRepository{db: db}
}

func (s *walletRepository) Create(ctx context.Context, wallet entity.Wallet) error {
	sqlStr := `INSERT INTO wallet ("owned_by", "status","balance") VALUES ($1, $2, $3)`
	var args []interface{}
	args = append(args, wallet.OwnedBy, wallet.Status, wallet.Balance)
	stmt, err := s.db.PrepareContext(ctx, sqlStr)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, args...)
	if err != nil {
		return err
	}
	return nil
}

func (s *walletRepository) Retrieve(ctx context.Context, username string) (wallet entity.Wallet, err error) {
	queryStr := `
	Select 
	    wa.id,
	    wa.owned_by,
	    wa.status,
	    COALESCE(wa.enabled_at, now()),
	    wa.balance
	from wallet wa 
	where wa.owned_by = $1`

	stmt, err := s.db.PrepareContext(ctx, queryStr)
	if err != nil {
		return entity.Wallet{}, err
	}
	err = stmt.QueryRowContext(ctx, username).Scan(
		&wallet.Id,
		&wallet.OwnedBy,
		&wallet.Status,
		&wallet.EnabledAt,
		&wallet.Balance)

	if err != nil {
		return entity.Wallet{}, err
	}
	return wallet, nil
}

func (s *walletRepository) UpdateWallet(ctx context.Context, customerId string, time time.Time, status bool) error {
	queryStr := `UPDATE wallet SET "status" = $1, `

	if status {
		queryStr += `"enabled_at" = $2 WHERE owned_by = $3`
	} else {
		queryStr += `"disabled_at" = $2 WHERE owned_by = $3`
	}

	var args []interface{}
	args = append(args, status, time, customerId)
	stmt, err := s.db.PrepareContext(ctx, queryStr)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, args...)
	if err != nil {
		return err
	}
	return nil
}

func (s *walletRepository) UpdateBalanceWallet(ctx context.Context, balance int32, customerId string) error {
	queryStr := `UPDATE wallet SET balance=$1 WHERE owned_by = $2`

	var args []interface{}
	args = append(args, balance, customerId)
	stmt, err := s.db.PrepareContext(ctx, queryStr)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, args...)
	if err != nil {
		return err
	}
	return nil
}
