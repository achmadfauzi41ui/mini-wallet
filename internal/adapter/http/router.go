package http

import (
	"github.com/gorilla/mux"
	"net/http"
)

func SetupRouter(
	router *mux.Router,
	handler *Handler,
) {

	router.Handle("/api/v1/init", handler.HandleInitWalletUser()).Methods(http.MethodPost)
	router.Handle("/api/v1/wallet", handler.HandleUpdateEnabledWalletUser()).Methods(http.MethodPost)
	router.Handle("/api/v1/wallet", handler.HandleRetrieveWalletUser()).Methods(http.MethodGet)
	router.Handle("/api/v1/wallet/deposits", handler.HandleDepositWalletUser()).Methods(http.MethodPost)
	router.Handle("/api/v1/wallet/withdrawals", handler.HandleWithdrawalWalletUser()).Methods(http.MethodPost)
	router.Handle("/api/v1/wallet", handler.HandleUpdateDisabledWalletUser()).Methods(http.MethodPatch)
}
