package http

import (
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"julo/internal/model"
	"julo/internal/service"
	"julo/internal/utils"
	"net/http"
	"strconv"
)

type Handler struct {
	walletService     service.WalletService
	depositService    service.DepositService
	withdrawalService service.WithdrawalService
}

func NewHandler(walletService service.WalletService, depositService service.DepositService, withdrawalService service.WithdrawalService) *Handler {
	return &Handler{walletService: walletService, depositService: depositService, withdrawalService: withdrawalService}
}

func (s *Handler) HandleInitWalletUser() http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		ctx := req.Context()
		err := req.ParseMultipartForm(64)
		if err != nil {
			utils.RespondError(w, http.StatusBadRequest, "Request is not Valid")
			return
		}
		request := model.WalletRequest{
			CustomerXID: req.Form.Get("customer_xid"),
		}

		token, err := s.walletService.InitializeWallet(ctx, request)
		if err != nil {
			utils.RespondError(w, http.StatusInternalServerError, "Something error in our service")
			return
		}
		data := map[string]string{
			"token": token,
		}
		utils.RespondSuccess(w, http.StatusOK, data)
	}
}

func (s *Handler) HandleRetrieveWalletUser() http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		ctx := req.Context()

		userInfo := ctx.Value("userInfo").(jwt.MapClaims)

		request := model.WalletRequest{
			CustomerXID: fmt.Sprintf("%s", userInfo["customer_xid"]),
		}
		data, err := s.walletService.RetrieveWallet(ctx, request)
		if err != nil {
			utils.RespondError(w, http.StatusInternalServerError, "Something error in our service")
			return
		}
		response := map[string]model.WalletResponse{
			"wallet": data,
		}
		utils.RespondSuccess(w, http.StatusOK, response)
	}
}

func (s *Handler) HandleUpdateEnabledWalletUser() http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		ctx := req.Context()

		userInfo := ctx.Value("userInfo").(jwt.MapClaims)

		request := model.WalletRequest{
			CustomerXID: fmt.Sprintf("%s", userInfo["customer_xid"]),
		}
		data, err := s.walletService.EnabledWallet(ctx, request)
		if err != nil {
			utils.RespondError(w, http.StatusInternalServerError, "Something error in our service")
			return
		}
		response := map[string]model.WalletResponse{
			"wallet": data,
		}
		utils.RespondSuccess(w, http.StatusOK, response)
	}
}

func (s *Handler) HandleDepositWalletUser() http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		ctx := req.Context()
		userInfo := ctx.Value("userInfo").(jwt.MapClaims)
		err := req.ParseMultipartForm(64)
		if err != nil {
			utils.RespondError(w, http.StatusBadRequest, "Request is not Valid")
			return
		}

		amount, err := strconv.ParseInt(req.Form.Get("amount"), 10, 32)
		if err != nil {
			utils.RespondError(w, http.StatusBadRequest, "Request is not Valid")
			return
		}

		referenceId := req.Form.Get("reference_id")
		if !utils.IsValidUUID(referenceId) {
			utils.RespondError(w, http.StatusBadRequest, "Request is not Valid")
			return
		}

		request := model.DepositRequest{
			CustomerXID: fmt.Sprintf("%s", userInfo["customer_xid"]),
			Amount:      int32(amount),
			ReferenceId: referenceId,
		}

		data, err := s.depositService.DepositMoney(ctx, request)
		if err != nil {
			utils.RespondError(w, http.StatusInternalServerError, "Something error in our service")
			return
		}
		response := map[string]model.DepositResponse{
			"deposit": data,
		}
		utils.RespondSuccess(w, http.StatusOK, response)
	}
}

func (s *Handler) HandleWithdrawalWalletUser() http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		ctx := req.Context()
		userInfo := ctx.Value("userInfo").(jwt.MapClaims)
		err := req.ParseMultipartForm(64)
		if err != nil {
			utils.RespondError(w, http.StatusBadRequest, "Request is not Valid")
			return
		}

		amount, err := strconv.ParseInt(req.Form.Get("amount"), 10, 32)
		if err != nil {
			utils.RespondError(w, http.StatusBadRequest, "Request is not Valid")
			return
		}

		referenceId := req.Form.Get("reference_id")
		if !utils.IsValidUUID(referenceId) {
			utils.RespondError(w, http.StatusBadRequest, "Request is not Valid")
			return
		}

		request := model.WithdrawalRequest{
			CustomerXID: fmt.Sprintf("%s", userInfo["customer_xid"]),
			Amount:      int32(amount),
			ReferenceId: referenceId,
		}

		data, err := s.withdrawalService.WithdrawalMoney(ctx, request)
		if err != nil {
			utils.RespondError(w, http.StatusInternalServerError, "Something error in our service")
			return
		}
		response := map[string]model.WithdrawalResponse{
			"deposit": data,
		}
		utils.RespondSuccess(w, http.StatusOK, response)
	}
}

func (s *Handler) HandleUpdateDisabledWalletUser() http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		ctx := req.Context()

		userInfo := ctx.Value("userInfo").(jwt.MapClaims)
		err := req.ParseMultipartForm(64)
		if err != nil {
			utils.RespondError(w, http.StatusBadRequest, "Request is not Valid")
			return
		}

		disabled, err := strconv.ParseBool(req.Form.Get("is_disabled"))
		if err != nil || !disabled {
			utils.RespondError(w, http.StatusBadRequest, "Request is not Valid")
			return
		}

		request := model.WalletRequest{
			CustomerXID: fmt.Sprintf("%s", userInfo["customer_xid"]),
		}
		data, err := s.walletService.DisabledWallet(ctx, request)
		if err != nil {
			utils.RespondError(w, http.StatusInternalServerError, "Something error in our service")
			return
		}
		response := map[string]model.WalletDisabledResponse{
			"wallet": data,
		}
		utils.RespondSuccess(w, http.StatusOK, response)
	}
}
