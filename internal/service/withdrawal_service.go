package service

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"julo/internal/adapter/repository"
	"julo/internal/model"
	"julo/internal/model/entity"
	"time"
)

type withdrawalService struct {
	walletRepository     repository.WalletRepository
	withdrawalRepository repository.WithdrawalRepository
}

func NewWithdrawalService(walletRepository repository.WalletRepository, withdrawalRepository repository.WithdrawalRepository) WithdrawalService {
	return &withdrawalService{walletRepository: walletRepository, withdrawalRepository: withdrawalRepository}
}

type WithdrawalService interface {
	WithdrawalMoney(ctx context.Context, request model.WithdrawalRequest) (model.WithdrawalResponse, error)
}

func (s *withdrawalService) WithdrawalMoney(ctx context.Context, request model.WithdrawalRequest) (model.WithdrawalResponse, error) {
	data, err := s.walletRepository.Retrieve(ctx, request.CustomerXID)
	if err != nil {
		return model.WithdrawalResponse{}, err
	} else if !data.Status {
		return model.WithdrawalResponse{}, errors.New("wallet currently not enabled")
	} else {
		count, err := s.withdrawalRepository.CheckReferenceId(ctx, request.ReferenceId)
		if err != nil || count > 0 {
			return model.WithdrawalResponse{}, errors.New("reference Id is not valid or unique")
		}
		withdrawal := entity.Withdrawal{
			Id:          uuid.New().String(),
			WithdrawnBy: request.CustomerXID,
			Status:      "success",
			WithdrawnAt: time.Now(),
			Amount:      request.Amount,
			ReferenceId: request.ReferenceId,
		}
		if data.Balance < request.Amount {
			withdrawal.Status = "failed"
			err = s.withdrawalRepository.Create(ctx, withdrawal)
			return model.WithdrawalResponse{
				Id:          withdrawal.Id,
				DepositedBy: withdrawal.WithdrawnBy,
				Status:      withdrawal.Status,
				DepositedAt: withdrawal.WithdrawnAt.UTC().Format(time.RFC3339),
				Amount:      withdrawal.Amount,
				ReferenceId: withdrawal.ReferenceId,
			}, nil
		}
		finalBalance := data.Balance - request.Amount
		err = s.walletRepository.UpdateBalanceWallet(ctx, finalBalance, request.CustomerXID)
		if err != nil {
			withdrawal.Status = "failed"
			err = s.withdrawalRepository.Create(ctx, withdrawal)
			if err != nil {
				return model.WithdrawalResponse{}, err
			}
		} else {
			withdrawal.Status = "success"
			err = s.withdrawalRepository.Create(ctx, withdrawal)
		}
		return model.WithdrawalResponse{
			Id:          withdrawal.Id,
			DepositedBy: withdrawal.WithdrawnBy,
			Status:      withdrawal.Status,
			DepositedAt: withdrawal.WithdrawnAt.UTC().Format(time.RFC3339),
			Amount:      withdrawal.Amount,
			ReferenceId: withdrawal.ReferenceId,
		}, nil
	}
}
