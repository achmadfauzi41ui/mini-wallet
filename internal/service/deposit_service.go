package service

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"julo/internal/adapter/repository"
	"julo/internal/model"
	"julo/internal/model/entity"
	"time"
)

type depositService struct {
	walletRepository  repository.WalletRepository
	depositRepository repository.DepositRepository
}

func NewDepositService(walletRepository repository.WalletRepository, depositRepository repository.DepositRepository) DepositService {
	return &depositService{walletRepository: walletRepository, depositRepository: depositRepository}
}

type DepositService interface {
	DepositMoney(ctx context.Context, request model.DepositRequest) (model.DepositResponse, error)
}

func (s *depositService) DepositMoney(ctx context.Context, request model.DepositRequest) (model.DepositResponse, error) {
	data, err := s.walletRepository.Retrieve(ctx, request.CustomerXID)
	if err != nil {
		return model.DepositResponse{}, err
	} else if !data.Status {
		return model.DepositResponse{}, errors.New("wallet currently not enabled")
	} else {
		count, err := s.depositRepository.CheckReferenceId(ctx, request.ReferenceId)
		if err != nil || count > 0 {
			return model.DepositResponse{}, errors.New("reference Id is not valid or unique")
		}
		deposit := entity.Deposit{
			Id:          uuid.New().String(),
			DepositedBy: request.CustomerXID,
			Status:      "success",
			DepositedAt: time.Now(),
			Amount:      request.Amount,
			ReferenceId: request.ReferenceId,
		}
		finalBalance := data.Balance + request.Amount
		err = s.walletRepository.UpdateBalanceWallet(ctx, finalBalance, request.CustomerXID)
		if err != nil {
			deposit.Status = "failed"
			err = s.depositRepository.Create(ctx, deposit)
			if err != nil {
				return model.DepositResponse{}, err
			}
		} else {
			deposit.Status = "success"
			err = s.depositRepository.Create(ctx, deposit)
		}
		return model.DepositResponse{
			Id:          deposit.Id,
			DepositedBy: deposit.DepositedBy,
			Status:      deposit.Status,
			DepositedAt: deposit.DepositedAt.UTC().Format(time.RFC3339),
			Amount:      deposit.Amount,
			ReferenceId: deposit.ReferenceId,
		}, nil
	}
}
