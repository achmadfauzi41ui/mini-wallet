package service

import (
	"context"
	"errors"
	"github.com/golang-jwt/jwt/v4"
	"julo/internal/adapter/repository"
	"julo/internal/model"
	"julo/internal/model/entity"
	"time"
)

type WalletService interface {
	InitializeWallet(ctx context.Context, userRequest model.WalletRequest) (string, error)
	RetrieveWallet(ctx context.Context, userRequest model.WalletRequest) (model.WalletResponse, error)
	EnabledWallet(ctx context.Context, userRequest model.WalletRequest) (model.WalletResponse, error)
	DisabledWallet(ctx context.Context, userRequest model.WalletRequest) (model.WalletDisabledResponse, error)
}

type walletService struct {
	walletRepository repository.WalletRepository
}

func NewWalletService(walletRepository repository.WalletRepository) WalletService {
	return &walletService{walletRepository: walletRepository}
}

func (s *walletService) InitializeWallet(ctx context.Context, walletRequest model.WalletRequest) (string, error) {
	_, err := s.walletRepository.Retrieve(ctx, walletRequest.CustomerXID)
	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			wallet := entity.Wallet{
				OwnedBy: walletRequest.CustomerXID,
				Status:  false,
				Balance: 0,
			}
			err = s.walletRepository.Create(ctx, wallet)
			if err != nil {
				return "", err
			}
			return generateToken(walletRequest.CustomerXID)
		} else {
			return "", err
		}
	} else {
		return generateToken(walletRequest.CustomerXID)
	}
}

func (s *walletService) RetrieveWallet(ctx context.Context, walletRequest model.WalletRequest) (model.WalletResponse, error) {
	data, err := s.walletRepository.Retrieve(ctx, walletRequest.CustomerXID)
	if err != nil {
		return model.WalletResponse{}, err
	} else {
		if !data.Status {
			response := model.WalletResponse{
				Status: "disabled",
			}
			return response, nil
		} else {
			response := model.WalletResponse{
				Id:        data.Id,
				OwnedBy:   data.OwnedBy,
				Status:    "enabled",
				EnabledAt: data.EnabledAt.UTC().Format(time.RFC3339),
				Balance:   data.Balance,
			}
			return response, nil
		}
	}
}

func (s *walletService) EnabledWallet(ctx context.Context, walletRequest model.WalletRequest) (model.WalletResponse, error) {
	data, err := s.walletRepository.Retrieve(ctx, walletRequest.CustomerXID)
	if err != nil {
		return model.WalletResponse{}, err
	} else {
		if !data.Status {
			currentTime := time.Now()
			err = s.walletRepository.UpdateWallet(ctx, walletRequest.CustomerXID, currentTime, true)
			if err != nil {
				return model.WalletResponse{}, err
			}
			response := model.WalletResponse{
				Id:        data.Id,
				OwnedBy:   data.OwnedBy,
				Status:    "enabled",
				EnabledAt: currentTime.UTC().Format(time.RFC3339),
				Balance:   data.Balance,
			}
			return response, nil
		} else {
			return model.WalletResponse{}, errors.New("wallet already enabled")
		}
	}
}

func (s *walletService) DisabledWallet(ctx context.Context, walletRequest model.WalletRequest) (model.WalletDisabledResponse, error) {
	data, err := s.walletRepository.Retrieve(ctx, walletRequest.CustomerXID)
	if err != nil {
		return model.WalletDisabledResponse{}, err
	} else {
		if data.Status {
			currentTime := time.Now()
			err = s.walletRepository.UpdateWallet(ctx, walletRequest.CustomerXID, currentTime, false)
			if err != nil {
				return model.WalletDisabledResponse{}, err
			}
			response := model.WalletDisabledResponse{
				Id:         data.Id,
				OwnedBy:    data.OwnedBy,
				Status:     "disabled",
				DisabledAt: currentTime.UTC().Format(time.RFC3339),
				Balance:    data.Balance,
			}
			return response, nil
		} else {
			return model.WalletDisabledResponse{}, errors.New("wallet already disabled")
		}
	}
}

func generateToken(customerXID string) (string, error) {
	claims := model.MyClaims{
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer:    "fauzi",
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(24 * time.Hour)),
		},
		CustomerXID: customerXID,
	}

	token := jwt.NewWithClaims(
		JWT_SIGNING_METHOD,
		claims,
	)

	signedToken, err := token.SignedString(JWT_SIGNATURE_KEY)

	if err != nil {
		return "", err
	}
	return signedToken, nil
}
