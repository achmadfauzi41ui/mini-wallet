package model

type WithdrawalRequest struct {
	CustomerXID string `json:"customerXID"`
	Amount      int32  `json:"amount"`
	ReferenceId string `json:"referenceId"`
}
