package model

type WalletResponse struct {
	Id        string `json:"id"`
	OwnedBy   string `json:"owned_by"`
	Status    string `json:"status"`
	EnabledAt string `json:"enabled_at"`
	Balance   int32  `json:"balance"`
}
