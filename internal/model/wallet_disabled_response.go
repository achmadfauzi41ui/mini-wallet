package model

type WalletDisabledResponse struct {
	Id         string `json:"id"`
	OwnedBy    string `json:"owned_by"`
	DisabledAt string `json:"disabled_at"`
	Status     string `json:"status"`
	Balance    int32  `json:"balance"`
}
