package entity

import "time"

type Wallet struct {
	Id         string    `json:"id" db:"id"`
	OwnedBy    string    `json:"owned_by" db:"owned_by"`
	Status     bool      `json:"status" db:"status"`
	EnabledAt  time.Time `json:"enabled_at" db:"enabled_at"`
	DisabledAt time.Time `json:"disabled_at" db:"disabled_at"`
	Balance    int32     `json:"balance" db:"balance"`
}
