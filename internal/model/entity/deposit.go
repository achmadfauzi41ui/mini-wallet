package entity

import "time"

type Deposit struct {
	Id          string    `json:"id" db:"id"`
	DepositedBy string    `json:"deposited_by" db:"deposited_by"`
	Status      string    `json:"status" db:"status"`
	DepositedAt time.Time `json:"deposited_at" db:"deposited_at"`
	Amount      int32     `json:"amount" db:"amount"`
	ReferenceId string    `json:"reference_id" db:"reference_id"`
}
