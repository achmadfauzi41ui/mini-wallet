package entity

import "time"

type Withdrawal struct {
	Id          string    `json:"id" db:"id"`
	WithdrawnBy string    `json:"withdrawn_by" db:"withdrawn_by"`
	Status      string    `json:"status" db:"status"`
	WithdrawnAt time.Time `json:"withdrawn_at" db:"withdrawn_at"`
	Amount      int32     `json:"amount" db:"amount"`
	ReferenceId string    `json:"reference_id" db:"reference_id"`
}
