package model

type DepositResponse struct {
	Id          string `json:"id"`
	DepositedBy string `json:"deposited_by"`
	Status      string `json:"status"`
	DepositedAt string `json:"deposited_at"`
	Amount      int32  `json:"amount"`
	ReferenceId string `json:"reference_id"`
}
