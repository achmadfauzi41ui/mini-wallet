package model

type WithdrawalResponse struct {
	Id          string `json:"id"`
	DepositedBy string `json:"withdrawn_by"`
	Status      string `json:"status"`
	DepositedAt string `json:"withdrawn_at"`
	Amount      int32  `json:"amount"`
	ReferenceId string `json:"reference_id"`
}
