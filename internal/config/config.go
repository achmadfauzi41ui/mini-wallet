package config

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/viper"
	"log"
	"strings"
	"sync"
	"time"
)

type Config struct {
	Env      string         `mapstructure:"env"`
	Port     int            `mapstructure:"port"`
	Postgres PostgresConfig `mapstructure:"postgres"`
}

type PostgresConfig struct {
	ConnMaxLifetime    int  `mapstructure:"connMaxLifetime"`
	MaxOpenConnections int  `mapstructure:"maxOpenConnections"`
	MaxIdleConnections int  `mapstructure:"maxIdleConnections"`
	ConnectTimeout     int  `mapstructure:"connectTimeout"`
	Master             PSQL `mapstructure:"master"`
}

type PSQL struct {
	Host     string `mapstructure:"host"`
	Port     int    `mapstructure:"port"`
	Schema   string `mapstructure:"schema"`
	DBName   string `mapstructure:"dbName"`
	User     string `mapstructure:"user"`
	Password string `mapstructure:"password"`
}

func New() (conf Config) {
	var once sync.Once
	once.Do(func() {
		v := viper.New()
		retried := 0
		err := InitialiseRemote(v, retried)
		if err != nil {
			log.Printf("No remote server configured will load configuration from file and environment variables: %+v", err)
			if err := InitialiseFileAndEnv(v, "config.local"); err != nil {
				if _, ok := err.(viper.ConfigFileNotFoundError); ok {
					configFileName := fmt.Sprintf("%s.yaml", "config.local")
					log.Printf("No '" + configFileName + "' file found on search paths. Will either use environment variables or defaults")
				} else {
					log.Fatalf("Error occured during loading config: %s", err.Error())
				}
			}
		}
		err = v.Unmarshal(&conf)
		if err != nil {
			log.Fatalf("%v", err)
		}
	})
	return conf
}

func InitialiseRemote(v *viper.Viper, retried int) error {
	v.SetConfigType("yaml")
	err := v.ReadRemoteConfig()
	if err != nil && retried < 1 {
		time.Sleep(500 * time.Millisecond)
		return InitialiseRemote(v, retried+1)
	}
	return err
}

func InitialiseFileAndEnv(v *viper.Viper, configName string) error {
	var searchPath = []string{
		"/etc/julo",
		"$HOME/.julo",
		".",
	}
	v.SetConfigName(configName)
	for _, path := range searchPath {
		v.AddConfigPath(path)
	}
	v.SetEnvPrefix("julo")
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	v.AutomaticEnv()
	return v.ReadInConfig()
}

func SetupDB(conf Config) *sqlx.DB {
	log.Printf("Connecting to postgresql (master) %s", fmt.Sprintf("host=%s user=%s dbname=%s sslmode=%s",
		conf.Postgres.Master.Host, conf.Postgres.Master.User, conf.Postgres.Master.DBName, "disable"))

	masterDB, err := sqlx.Open("postgres",
		fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s connect_timeout=%d",
			conf.Postgres.Master.Host, conf.Postgres.Master.Port, conf.Postgres.Master.User, conf.Postgres.Master.Password, conf.Postgres.Master.DBName, "disable", conf.Postgres.ConnectTimeout))
	if err != nil {
		log.Fatalf("open db connection failed (master) %v", err)
	}
	masterDB.SetMaxOpenConns(conf.Postgres.MaxOpenConnections)
	masterDB.SetMaxIdleConns(conf.Postgres.MaxIdleConnections)
	masterDB.SetConnMaxLifetime(time.Duration(conf.Postgres.ConnMaxLifetime) * time.Millisecond)
	if err := masterDB.Ping(); err != nil {
		log.Fatalf("ping db connection failed (master) %v", err)
	}

	return masterDB
}
