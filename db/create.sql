CREATE TABLE "wallet" (
  "id" uuid DEFAULT gen_random_uuid() PRIMARY KEY,
  "owned_by" uuid NOT NULL,
  "status" bool,
  "enabled_at" timestamptz,
  "disabled_at" timestamptz,
  "balance" int4
);
CREATE TABLE "deposit" (
    "id" uuid PRIMARY KEY, "deposited_by" uuid NOT NULL,
    "status" varchar not null, "deposited_at" timestamptz,
    "amount" int4, "reference_id" uuid not null
);
CREATE TABLE "withdrawal" (
  "id" uuid PRIMARY KEY, "withdrawn_by" uuid NOT NULL,
  "status" varchar not null, "withdrawn_at" timestamptz,
  "amount" int4, "reference_id" uuid not null
);
